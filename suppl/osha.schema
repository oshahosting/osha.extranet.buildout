# OpenLDAP Schema extension for OSHA project
# OID Prefix: 1.3.6.1.4.1.20708. 
#
# OID 20708 assigned private enterprise number for SYSLAB.COM GmbH
# OID Prefix.2    : LDAP 
# OID Prefix.2.2.x: Attribute Types
# OID Prefix.2.6.x: Auxiliary objectclasses
# OID Prefix.2.7.x: Structural objectclasses
# Postfix .v denotes version of definition

# v1.0 - 2004/06/29 - SYSLAB.COM GmbH
# v1.1 - 2004/12/13 - SYSLAB.COM GmbH
# v1.2 - 2005/01/25 - SYSLAB.COM GmbH
# v1.3 - 2010/04/22 - SYSLAB.COM GmbH

# Attributes for OSHA Member (Range 2000 - 2100)
# derived from "workingonsafety.net" user profiles
attributetype ( 1.3.6.1.4.1.20708.2.2.2000.1 NAME 'oshPersProfile' 
	DESC 'CV-abstract for personal profile' 
        EQUALITY caseIgnoreMatch
        SUBSTR caseIgnoreSubstringsMatch
	SYNTAX '1.3.6.1.4.1.1466.115.121.1.15{1600}' 
	SINGLE-VALUE )
	
attributetype ( 1.3.6.1.4.1.20708.2.2.2001.1 NAME 'oshPersEducation' 
	DESC 'Description of educational backgroud (Personal Profile)' 
        EQUALITY caseIgnoreMatch
        SUBSTR caseIgnoreSubstringsMatch
	SYNTAX '1.3.6.1.4.1.1466.115.121.1.15{1600}' 
	SINGLE-VALUE )
	
attributetype ( 1.3.6.1.4.1.20708.2.2.2002.1 NAME 'oshPersMemberships' 
	DESC 'Organisational memberships  (Personal Profile)' 
        EQUALITY caseIgnoreMatch
        SUBSTR caseIgnoreSubstringsMatch
	SYNTAX '1.3.6.1.4.1.1466.115.121.1.15{1600}' 
	SINGLE-VALUE )
	
attributetype ( 1.3.6.1.4.1.20708.2.2.2003.1 NAME 'oshPersRewards' 
	DESC 'Rewards (Personal Profile)' 
        EQUALITY caseIgnoreMatch
        SUBSTR caseIgnoreSubstringsMatch
	SYNTAX '1.3.6.1.4.1.1466.115.121.1.15{1600}' 
	SINGLE-VALUE )
	
attributetype ( 1.3.6.1.4.1.20708.2.2.2004.1 NAME 'oshPersProjects' 
	DESC 'Project list (Personal Profile)' 
        EQUALITY caseIgnoreMatch
        SUBSTR caseIgnoreSubstringsMatch
	SYNTAX '1.3.6.1.4.1.1466.115.121.1.15{1600}' 
	SINGLE-VALUE )
	
attributetype ( 1.3.6.1.4.1.20708.2.2.2005.1 NAME 'oshPersPublications' 
	DESC 'List of publications (Personal Profile)' 
        EQUALITY caseIgnoreMatch
        SUBSTR caseIgnoreSubstringsMatch
	SYNTAX '1.3.6.1.4.1.1466.115.121.1.15{4100}' 
	SINGLE-VALUE )
	
attributetype ( 1.3.6.1.4.1.20708.2.2.2006.1 NAME 'oshPersLanguages' 
	DESC 'Languages spoken (Personal Profile)' 
        EQUALITY caseIgnoreMatch
        SUBSTR caseIgnoreSubstringsMatch
	SYNTAX '1.3.6.1.4.1.1466.115.121.1.15' )
	
attributetype ( 1.3.6.1.4.1.20708.2.2.2007.1 NAME 'oshPersProfession' 
	DESC 'Profession of member (Personal Profile)' 
        EQUALITY caseIgnoreMatch
        SUBSTR caseIgnoreSubstringsMatch
	SYNTAX '1.3.6.1.4.1.1466.115.121.1.15{}' 
	SINGLE-VALUE )
	
attributetype ( 1.3.6.1.4.1.20708.2.2.2008.1 NAME 'oshPersSubjects' 
	DESC 'Subjects member is interested in (Personal Profile)' 
        EQUALITY caseIgnoreMatch
        SUBSTR caseIgnoreSubstringsMatch
	SYNTAX '1.3.6.1.4.1.1466.115.121.1.15' )

attributetype ( 1.3.6.1.4.1.20708.2.2.2009.1 NAME 'oshPersCountryPreference' 
	DESC 'Preferred country' 
        EQUALITY caseIgnoreMatch
        SUBSTR caseIgnoreSubstringsMatch
	SYNTAX '1.3.6.1.4.1.1466.115.121.1.15' )

attributetype ( 1.3.6.1.4.1.20708.2.2.2010.1 NAME 'oshPersOrganizationSize'
        DESC 'Size of organization'
        EQUALITY caseIgnoreMatch
        SUBSTR caseIgnoreSubstringsMatch
        SYNTAX '1.3.6.1.4.1.1466.115.121.1.15' 
	SINGLE-VALUE )

attributetype ( 1.3.6.1.4.1.20708.2.2.2011.1 NAME 'oshPersOrganizationSector'
        DESC 'Business sector of organization'
        EQUALITY caseIgnoreMatch
        SUBSTR caseIgnoreSubstringsMatch
        SYNTAX '1.3.6.1.4.1.1466.115.121.1.15' )

#  Attributes for OSHA Staff (Range 2100 - 2200)
attributetype ( 1.3.6.1.4.1.20708.2.2.2100.1 NAME 'oshStaffUploadSource' 
	DESC 'Description of Upload Source' 
        EQUALITY caseIgnoreMatch
        SUBSTR caseIgnoreSubstringsMatch
	SYNTAX '1.3.6.1.4.1.1466.115.121.1.15' 
	SINGLE-VALUE )
	
attributetype ( 1.3.6.1.4.1.20708.2.2.2101.1 NAME 'oshStaffFocalPoint' 
	DESC 'Focal Point Staff belongs to' 
        EQUALITY caseIgnoreMatch
        SUBSTR caseIgnoreSubstringsMatch
	SYNTAX '1.3.6.1.4.1.1466.115.121.1.15' 
	SINGLE-VALUE )
	
attributetype ( 1.3.6.1.4.1.20708.2.2.2102.1 NAME 'oshStaffPosition' 
	DESC 'Position' 
        EQUALITY caseIgnoreMatch
        SUBSTR caseIgnoreSubstringsMatch
	SYNTAX '1.3.6.1.4.1.1466.115.121.1.15' 
	SINGLE-VALUE )

attributetype ( 1.3.6.1.4.1.20708.2.2.2103.1 NAME 'oshPersInterests'
	DESC 'User Interests'
	EQUALITY caseIgnoreMatch
	SUBSTR caseIgnoreSubstringsMatch
	SYNTAX '1.3.6.1.4.1.1466.115.121.1.15')

attributetype ( 1.3.6.1.4.1.20708.2.2.2200.1 NAME 'PasswordChallenge'
        DESC 'Challenge phrase for automated password reset.'
        EQUALITY caseIgnoreMatch
        SUBSTR caseIgnoreSubstringsMatch
        SYNTAX '1.3.6.1.4.1.1466.115.121.1.15{120}'
        SINGLE-VALUE )

attributetype ( 1.3.6.1.4.1.20708.2.2.2201.1 NAME 'PasswordResponse'
        DESC 'Response for challenge phrase'
        EQUALITY caseIgnoreMatch
        SUBSTR caseIgnoreSubstringsMatch
        SYNTAX '1.3.6.1.4.1.1466.115.121.1.15{120}'
        SINGLE-VALUE )

# Attributes for OSHA Extranet members (Range 2200 - 2300)
# derived from http://extranet.osha.europa.eu/livelink/ user profiles

attributetype ( 1.3.6.1.4.1.20708.2.2.2300.1 NAME 'ictAlternativeMail'
        DESC 'Alternative E-mail'
        EQUALITY caseIgnoreIA5Match
        SUBSTR caseIgnoreIA5SubstringsMatch
        SYNTAX '1.3.6.1.4.1.1466.115.121.1.26{256}'
        SINGLE-VALUE )

attributetype ( 1.3.6.1.4.1.20708.2.2.2301.1 NAME 'ictBirthday' 
	DESC 'Birthday' 
        EQUALITY caseIgnoreMatch
        SUBSTR caseIgnoreSubstringsMatch
	SYNTAX '1.3.6.1.4.1.1466.115.121.1.15{1600}' 
	SINGLE-VALUE )

attributetype ( 1.3.6.1.4.1.20708.2.2.2302.1 NAME 'ictGender' 
	DESC 'Gender' 
        EQUALITY caseIgnoreMatch
        SUBSTR caseIgnoreSubstringsMatch
	SYNTAX '1.3.6.1.4.1.1466.115.121.1.15{256}' 
	SINGLE-VALUE )

attributetype ( 1.3.6.1.4.1.20708.2.2.2303.1 NAME 'ictFavoriteLinks'
        DESC 'My Favorite Links'
        EQUALITY caseIgnoreIA5Match
        SUBSTR caseIgnoreIA5SubstringsMatch
        SYNTAX '1.3.6.1.4.1.1466.115.121.1.26' )

attributetype ( 1.3.6.1.4.1.20708.2.2.2304.1 NAME 'ictMyHomepage'
        DESC 'My Homepage'
        EQUALITY caseIgnoreIA5Match
        SUBSTR caseIgnoreIA5SubstringsMatch
        SYNTAX '1.3.6.1.4.1.1466.115.121.1.26' 
        SINGLE-VALUE )

attributetype ( 1.3.6.1.4.1.20708.2.2.2305.1 NAME 'ictTimeZone'
        DESC 'Time Zone'
        EQUALITY caseIgnoreMatch
        SUBSTR caseIgnoreSubstringsMatch
        SYNTAX '1.3.6.1.4.1.1466.115.121.1.15'
        SINGLE-VALUE )

attributetype ( 1.3.6.1.4.1.20708.2.2.2306.1 NAME ( 'ictHomeFacsimileTelephoneNumber' 'ictHomeFax' )
        DESC 'Home fax'
        SYNTAX '1.3.6.1.4.1.1466.115.121.1.22' )


# Objectclasses
	
# Structural classes
	
# Auxilliary classes
	
objectclass ( 1.3.6.1.4.1.20708.2.6.100.1 NAME 'oshPerson' AUXILIARY 
	MAY ( oshPersProfile $ oshPersEducation $ oshPersMemberships $ oshPersRewards $ oshPersProjects $ oshPersInterests
			$ oshPersPublications $ oshPersLanguages $ oshPersProfession $ oshPersSubjects $ oshPersCountryPreference
			$ oshPersOrganizationSize $ oshPersOrganizationSector $ PasswordChallenge $ PasswordResponse
	 ) )
 			
objectclass ( 1.3.6.1.4.1.20708.2.6.101.1 NAME 'oshStaff' AUXILIARY
	MAY ( oshStaffUploadSource $ oshStaffFocalPoint $ oshStaffPosition ) )  

objectclass ( 1.3.6.1.4.1.20708.2.6.102.1 NAME 'ictPerson' AUXILIARY
	MAY ( ictAlternativeMail $ ictBirthday $ ictGender $ ictFavoriteLinks $ ictMyHomepage $ ictTimeZone 
	                $ ictHomeFacsimileTelephoneNumber $ oshPersSubjects) )  
