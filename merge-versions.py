from ConfigParser import RawConfigParser
import re

def mycmp(version1, version2):
    def normalize(v):
        return [int(x) for x in re.sub(r'(\.0+)*$','', v).split(".")]
    try:
        x = cmp(normalize(version1), normalize(version2))
        return x
    except:
        print "CHECK MANUALLY: couldn't compare %s and %s, returning the first" % (version1, version2)
        return 1

def read_cfg(filename):
    cfg = RawConfigParser()
    cfg.optionxform = str
    cfg.readfp(open(filename))
    return cfg

def merge_versions(newconf):
    for item in newconf.items('versions'):
        if not v_orig.has_option('versions', item[0]):
            v_orig.set('versions', item[0], item[1])
        else:
            x = mycmp(newconf.get('versions', item[0]), v_orig.get('versions', item[0]))
            if x == 1:
                v_orig.set('versions', item[0], item[1])
            elif x == -1:
                print "INFO: Current version bigger than future for %s" % item[0]
            else:
                print "INFO: No change for %s" % item[0]

v_orig = read_cfg('versions.cfg')
v_425 = read_cfg('versions-4.2.5.cfg')
v_zope = read_cfg('versions-zope2.cfg')
v_zopeapp = read_cfg('versions-zopeapp.cfg')
v_ztk = read_cfg('versions-ztk.cfg')

merge_versions(v_zopeapp)
merge_versions(v_ztk)
merge_versions(v_zope)
merge_versions(v_425)

v_orig.write(open('versions-new.cfg', 'w'))

print "done"
